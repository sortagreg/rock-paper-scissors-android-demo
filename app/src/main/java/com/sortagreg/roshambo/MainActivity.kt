package com.sortagreg.roshambo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController

class MainActivity : AppCompatActivity() {

    // NavController is part of Android Navigation Component Library and is part of "Jetpack".
    // https://www.youtube.com/watch?v=ELGShpd17wc
    // It's a longer video(37m), but teaches you everything you need to know to use this effectively
    // and better than I can explain it for sure.
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navController = findNavController(R.id.navHostFragment)
        setupActionBarWithNavController(navController)
    }

    // Makes sure the toolbar shows the correct back icon and behaves as expected.
    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}
