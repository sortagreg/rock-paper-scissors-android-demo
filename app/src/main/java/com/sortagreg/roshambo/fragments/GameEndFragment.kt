package com.sortagreg.roshambo.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.sortagreg.roshambo.R
import kotlin.random.Random

class GameEndFragment: Fragment() {

    // This is how you retrieve the arguments sent via SageArgs from the GameStartFragment.
    private val arg: GameEndFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_game_end, container, false)

        val button = view.findViewById<Button>(R.id.buttonPlayAgain)

        // This is where you retrieve the specific argument you passed to this fragment.
        // The name of the what you retrieve is specified by you in the Navigation XML.
        val userEntry = arg.userEntry

        val aiEntry = Random.nextInt(1, 3)

        val userThrow = view.findViewById<TextView>(R.id.textViewPlayerChoice)
        val aiThrow = view.findViewById<TextView>(R.id.textViewAiChoice)
        val result = view.findViewById<TextView>(R.id.textViewReults)

        userThrow.text = setThrowText(userEntry)
        aiThrow.text = setThrowText(aiEntry)

        // The following result.text = accomplish the same thing. In fact, their method body's
        // basically are identical. setResultText() uses a standard method call, roShamBo() is a
        // Kotlin extension method.

//        result.text = setResultText(userEntry, aiEntry)
        result.text = userEntry.roShamBo(aiEntry)

        button.setOnClickListener {
            findNavController().popBackStack(R.id.startFragment, false)
        }

        return view
    }


    private fun setThrowText(entry: Int): String {
        return when (entry) {
            1 -> getString(R.string.throwRock)
            2 -> getString(R.string.throwPaper)
            3 -> getString(R.string.throwScissors)
            else -> getString(R.string.error_blank)
        }
    }

    // The following functions, essentially, do the same exact thing.

    // Normal function
    private fun setResultText(userThrow: Int, aiThrow: Int): String {
        return when (userThrow) {
            aiThrow -> "Tie"
            1 -> if (aiThrow == 2) getString(R.string.youLose) else getString(R.string.youWon)
            2 -> if (aiThrow == 3) getString(R.string.youLose) else getString(R.string.youWon)
            3 -> if (aiThrow == 1) getString(R.string.youLose) else getString(R.string.youWon)
            else -> getString(R.string.error_blank)
        }
    }

    // Kotlin Extension Function
    fun Int.roShamBo(aiThrow: Int): String {
        return when (this) {
            aiThrow -> "Tie"
            1 -> if (aiThrow == 2) getString(R.string.youLose) else getString(R.string.youWon)
            2 -> if (aiThrow == 3) getString(R.string.youLose) else getString(R.string.youWon)
            3 -> if (aiThrow == 1) getString(R.string.youLose) else getString(R.string.youWon)
            else -> getString(R.string.error_blank)
        }
    }

}