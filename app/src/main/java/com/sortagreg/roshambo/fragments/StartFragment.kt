package com.sortagreg.roshambo.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController

import com.sortagreg.roshambo.R


/**
 * A simple [Fragment] subclass.
 *
 */
class StartFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_start, container, false)

        val buttonPlay = view.findViewById<Button>(R.id.buttonPlay)

        buttonPlay.setOnClickListener {
            findNavController().navigate(R.id.action_startFragment_to_gameStartFragment)
        }

        return view
    }


}
